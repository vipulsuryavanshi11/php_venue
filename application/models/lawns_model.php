<?php
class Lawns_model extends CI_model
{
	public function __construct()
	{
		$this->load->database();
	}
	public function insert_for_booking($data)
	{

		$this->db->insert('Booking',$data);
		return "ok";
	}
	public function get_for_booking()
	{
		$this->db->select('checkin');
		$this->db->from('Booking');
		$query=$this->db->get();
		return $query;
	}
	public function insert_for_registration($other_feilds,$file_data)
	{
		$filess = array();
		$tt=implode(",",$file_data);
		$filess['images']=$tt;
		$all_form_data=array_merge($other_feilds,$filess);
		$this->db->insert("registration",$all_form_data);
		return "ok";
	}
	public function get_login_confirmation($username,$password)
	{
		$this->db->select('email,password,venue_name');
		$this->db->from('registration');
		$this->db->where('email',$username);
		$this->db->where('password',$password);
		$result=$this->db->get();
		return $result->row_array();

	}
	public function get_report_data($venue_name)
	{

		$this->db->select('firstname,lastname,email,phone,venue_name');
		$this->db->from('Booking');
		$this->db->where('venue_name',$venue_name);
		$result=$this->db->get();
		return $result->row_array();
	}
	public function get_registration_data()
	{	

		$this->db->select('*');
		$this->db->from('registration');
		//$this->db->where('venue_name',$pass);
		$result = $this->db->get();
		return $result->result();
	}
	public function get_search_data($search)
	{
		$this->db->select('*');
		$this->db->from('registration');
		$this->db->where('city',$search );
		// $this->db->or_where('max_budget<=',$budgetrange);
		$result = $this->db->get();
		return $result->result();
	}
}
?>
