<?php
class Lawns extends CI_controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('lawns_model');
	}
	public function index(){
		$this->load->view('lawns/header.html');
		$this->load->view('lawns/main.html');
		$this->load->view('lawns/footer.html');
		
	}
	public function BussinessLogin(){
		$this->load->view('lawns/header.html');
		$this->load->view('lawns/BussinessLogin.html');
		$this->load->view('lawns/footer.html');
	}
	public function BussinessReport(){
		$username=$_GET['username'];
		$password=$_GET['password'];
		$result=$this->lawns_model->get_login_confirmation($username,$password);
		//echo var_dump($result);
		$v_email=$result['email'];
		$v_psw=$result['password'];
		$venue_name=$result['venue_name'];
		// echo $venue_name;
		if ($username == $v_email AND $password== $v_psw) {
			$r_result=$this->lawns_model->get_report_data($venue_name);
			//echo var_dump($r_result);
			if(isset($r_result['venue_name']))
			{
				if($venue_name==$r_result['venue_name'])
				{
					$report_array=array('firstname' => $r_result['firstname'],'lastname'=>$r_result['lastname'],
						'email'=>$r_result['email'],'phone'=>$r_result['phone']);
					$this->load->view('lawns/header.html');
					$this->load->view('lawns/BussinessReport.html',$report_array);
				 	$this->load->view('lawns/footer.html');
				}
				else
				{
					echo "no one can register your venue";
				}
			}
			else
			{
				echo "no one can register your venue";
			}
		  }
		 else
		 	echo "enter wrong info";
	}

	public function main(){
	$this->load->view('lawns/header.html');
	$this->load->view('lawns/main.html');
	$this->load->view("lawns/footer.html");
	}
	public function register(){
		$this->load->view('lawns/register.html');
		
	}
	public function dashbord(){
			// $pass=$this->uri->segment(3);
	// print $pass;
		
		if (isset($_GET['search'])) {
			$search=$_GET['search'];
			// $budgetrange=$_GET['budgetrange'];
			$result=$this->lawns_model->get_search_data($search);
		}
		else{
		
			$send_data_array  = array();
			$result=$this->lawns_model->get_registration_data();
		}
		
		/*foreach ($result as $row) {
			$venues['venues']=$row->venue_name;
			array_push($web_sites['web_sites']=$row->web_site);
			//$img_files=$row->img_file;
			array_push($send_data_array, $venues,$web_sites);
		}*/
		//var_dump($result);
		/*$vv=json_encode($result);
		echo $vv['venue_name'];
		//var_dump($result);
		foreach($result as $key=>$object) {
			echo $object->venue_name."</br>";
			echo $object->address."</br>";
			# code...
		}*/
		

		$data['data']= $result;
		$this->load->view('lawns/dashbord.html',$data);
	}
	public function registration_form(){
	$counter = count($_FILES['img_file']['name']);
		echo $counter;
		//echo $_FILES['img_file']['name'][0];
		//echo $_FILES['img_file']['error'][1];
		$file_name_array = array();
		$array_for_file_vlaues = array();
	//	$post_data_array = array();
		for($i=0;$i<=$counter-1;$i++)
		{
			if(is_uploaded_file($_FILES['img_file']['tmp_name'][$i]) && $_FILES['img_file']['error'][$i]==0)
			{
				$file_path = '/var/www/html/codeigniter/static/image/'.$_FILES['img_file']['name'][$i];
				if (file_exists($file_path)) {
					echo "you alredy uploaded".$_FILES['img_file']['name'][$i]."file plz try another";	
				}
				else
				{
					if(move_uploaded_file($_FILES['img_file']['tmp_name'][$i],$file_path))
					{
						
						echo "uploaded successffully".$i."</br>";
						$permenent_name =  $_FILES['img_file']['name'][$i];
						$file_name_array[$i]=$_FILES['img_file']['name'][$i];

						//echo "111";
						//print_r($file_name_array);
						$ck=array_push($array_for_file_vlaues, $file_name_array[$i]);
						//echo "222";

					}
					else
					{
						echo "not uploaded".$i."</br>";
					}

				}
			}
			else
			{
				echo "file not uploaded due to";
			}
		}//end for

	//print_r($_POST);
	/*	foreach ($_POST as $key => $value) {
			
			array_push($post_data_array,array($key=>$value));
		}*/
		$post_data_array = array('venue_name'=>$_POST['venue_name'],'address'=>$_POST['address'],'state'=>$_POST['state']
			,'pin_code'=>$_POST['pin_code'],'mobile'=>$_POST['mobile'],'email'=>$_POST['email'],
			'web_site'=>$_POST['web_site'],'min_budget'=>$_POST['min_budget'],'max_cpacity'=>$_POST['max_cpacity'],
			'Description'=>$_POST['Description'],'city'=>$_POST['city'],'max_budget'=>$_POST['max_budget'],
			"password"=>$_POST['psw']);

		$result=$this->lawns_model->insert_for_registration($post_data_array,$array_for_file_vlaues);

	//	$this->load->view('lawns/main.html');
	// header('location: lawns/main');*/
		if ($result=="ok") {
			$pass=$_POST['venue_name'];
			redirect('lawns/dashbord');
			# code...
		}
		
		//$this->output->set_output(json_encode(array('venue_name'=>$_POST['venue_name'])));
	}

	public function booking(){
			$venue=$_GET['venue_name'];
			$data['data']=$venue;
			
		$this->load->view('lawns/booking',$data);
		
	}
	public function insert_booking(){
			

			$first_name = $_POST['first_name'];
			$last_name = $_POST['last_name'];
			$checkin = $_POST['checkin'];
			$checkout = $_POST['checkout'];
			$email = $_POST['email'];
			$phone = $_POST['ph_number'];
			$venue_name = $_POST['venue_name'];

			$booking_input = array(
				'firstname'=>$first_name,'lastname'=>$last_name,'email'=>$email,
				'phone'=>$phone,'checkin'=>$checkin,'checkout'=>$checkout,'venue_name'=>$venue_name);
		
			$result=$this->lawns_model->insert_for_booking($booking_input);
			
			if ($result=='ok') {
				echo "Hello"." " .$_POST['first_name']. " "."successfully registered";
			}
			
		
	}
	public function get_booking_check(){
		$checkin = $_GET['checkin'];
		$checkout = $_GET['checkout'];
		$getname=$this->lawns_model->get_for_booking();
		foreach($getname->result_array() as $row){
			$count=1;
				$time=strtotime($row['checkin']);
				if($_GET['checkin']==date("y-m-d",$time))
				{
					echo "we are sorry already booked!!! Try Another";
					$count=0;
					break;
				}
		}
		if($count){
			echo "Dates are Available!!";
		}


	}		
			
}
?>