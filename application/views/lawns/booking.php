<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>static/css/lib/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>static/css/lawns_home.css"/>
	
</head>
<body>
<div class="row">
	<div class="col-lg-12">
		<nav class="foropacity navbar-inverse navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        				<span class="icon-bar"></span>
       					<span class="icon-bar"></span>
       					<span class="icon-bar"></span>                        
     				</button>
					<a class="navbar-brand" href="#">Lawns</a>
				</div>
				<div class="collapse navbar-collapse" id="mynavbar">
					<ul class="nav navbar-nav navbar-right">
						<li><a class="underline_eff" href='<?php echo site_url('/lawns/index');?>'><strong class="menu_colr">Home</strong></a></li>
						<li class="dropdown">
							<a class="underline_eff" href='#' class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><strong class="menu_colr">Facilities</strong></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="<?php echo site_url("/lawns/dashbord");?>">venue</a></li>
								<li><a href="#">Mehendi</a></li>
								<li><a href="#">Photogrphy</a></li>
								<li><a href="#">Makeup</a></li>
								
							</ul>
						
						</li>
						<li><a class="underline_eff" href='#' class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><strong class="menu_colr">Event</strong></a></li>
						<li><a class="underline_eff" href='#'><strong class="menu_colr">Photo Gallery</strong></a></li>	
						<li><a class="underline_eff" href='<?php echo site_url('lawns/booking');?>'><strong class="menu_colr">Booking</strong></a></li>
						<li><a class="underline_eff" href='#'><strong class="menu_colr">Contact Us</strong></a></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>
</div>
<div class="row">
<div class="col-lg-12" style="height:60px"></div>	
</div>
<div class="row">
	<div class="col-lg-4">
	</div>
	<div class="col-lg-3 panel panel-default">
		<form class="panel-body" method="POST">
			<div class="form-group">
				<label for="text">Choose Date:</label>
				<div class="input-group date" id="datepicker1">
					<input type="text" class="form-control" placeholder="Check In" id="checkin" name="checkin" />
					<span class="input-group-addon">
	                   	<span class="glyphicon glyphicon-calendar">
	                   	</span>
	               	</span>
	            </div>
	        </div>
	        <div class="form-group">
	            <div class="input-group date" id="datepicker2">
					<input type="text" class="form-control" placeholder="Check Out" id="checkout" name="checkout" />
					<span class="input-group-addon">
	                   	<span class="glyphicon glyphicon-calendar"></span>
	                </span>
	           	</div>
			</div>
			<div class="form-group">
				<div class="input-group">
					<button type="btn" class="btn-success form-control" id="check">Check</button>		
				</div>
			</div>
			<div id="check_result">
				
			</div>
			<div>
				<div class="form-group">
					<label for="text">Frist Name:</label>
					<div class="input-group">
						<input type="text" class="form-control" id="first_name" name="first_name" />
					</div>	
				</div>
				<div class="form-group">
					<label for="text">Last Name:</label>
					<div class="input-group">
						<input type="text" class="form-control" id="last_name" name="last_name"/>
					</div>	
				</div>
				<div class="form-group">
					<label for="email">Email Address:</label>
					<div class="input-group">
						<input type="email" class="form-control" id="email" name="email"/>
					</div>	
				</div>
				<div class="form-group">
					<label for="text">Phone Number:</label>
					<div class="input-group">
						<input type="text" class="form-control" id="ph_number" name="ph_number"/>
					</div>	
				</div>
				<div class="form-group">
					<div class="input-group">
						<input type="hidden" class="form-control" value="<?php echo $data;?>" id="venue" name="venue"/>
					</div>	
				</div>

				<div class="form-group">
					
					<div class="input-group">
						<button type="btn" class="form-control btn-primary" id="submit_id">submit</button>
					</div>	
				</div>
				<div id="result">
					
				</div>

			</div>
		</form>
	</div>
	<div class="col-lg-1">

	</div>

	<div class="col-lg-4 verticalLine">
		<h3>Contact Us</h3>
			<dl>
				<dt><p>Address:</p></dt>
				<dd><p>Nagar Road, Near Kharadi Bypass <p>
				<p>Opp. Lohgaon-Wagholi Octroi Naka,Pune 411047</p>
				<dt><p>Phone numbers:</p></dt>
				<dd><p>9923673599/0000000000</p></dd>
				<dt><p>E-mail:</p></dt>
				<dd><p>vipulsuryavanshi11@gmail.com</p></dd>
	</div>
</div>
	<div class="well row">
		<div class="col-lg-8">
			<h3>Contact Us</h3>
			<dl>
				<dt><p>Address:</p></dt>
				<dd><p>Nagar Road, Near Kharadi Bypass <p>
				<p>Opp. Lohgaon-Wagholi Octroi Naka,Pune 411047</p>
				<dt><p>Phone numbers:</p></dt>
				<dd><p>9923673599/0000000000</p></dd>
				<dt><p>E-mail:</p></dt>
				<dd><p>vipulsuryavanshi11@gmail.com</p></dd>

		</div>
		<div class="col-lg-4">
			<h3>Map</h3>

			<div id="map" style="width:350px;height:350px;background:yellow">
			</div>
		</div>
	</div>

<script>
function myMap() {
  var mapCanvas = document.getElementById("map");
  var mapOptions = {
    center: new google.maps.LatLng(18.564727, 73.778869), zoom: 16
  };
  var map = new google.maps.Map(mapCanvas, mapOptions);
}
</script>



<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyCryCRnqf5tKcmKi-gpS4rHDWOM1GXsXQg&callback=myMap" async="" defer="defer" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url();?>static/js/jquery-2.2.0.js"></script>
<script type='text/javascript' src="<?php echo base_url(); ?>static/js/bootstrap.min.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<script type="text/javascript">


$(document).ready(function() {
    $("#datepicker1").datetimepicker({ format: "yyyy/mm/dd" });
});

</script>
<script>
	/*$(document).ready(function(){
	$("#submit_id").click(function(){
		var first_name = $('$first_name').val();
		/*$.post('lawns/insert_booking',{first_name:first_name},function(data){
			$('#result').html(data);
		$.ajax({
			url: 'insert_booking',
			data: {first_name:first_name},
			type: 'POST',
			success: function(data){
				$("#result").html(data);
			}
		});
		
	});
	});*/
	</script>
	  <script type="text/javascript">

        $("#submit_id").click(function(e) {
            e.preventDefault();
            var first_name = $("#first_name").val();
            var last_name = $("#last_name").val();
            var email = $("#email").val();
            var email = $("#email").val();
            var ph_number = $('#ph_number').val();
            var checkin = $("#checkin").val();
            var checkout = $("#checkout").val();
            var venue_name = $('#venue').val();

          // 	$('form').reset();
            
            $.ajax({
                url: "<?php echo site_url('lawns/insert_booking'); ?>",
                method: "POST",
                data: {first_name: first_name,last_name:last_name,email:email,ph_number:ph_number,
                	checkin:checkin,checkout:checkout,venue_name:venue_name},
                success: function(data) {
                 	$("#result").html(data);
                 	$("#first_name").val("");
          			$("#last_name").val("");
          			$("#email").val("");
          			$('#ph_number').val("");
          			$("#checkin").val("");
          			$("#checkout").val("");
          			$("#venue").val("");
                }
            });
        });
	
		$("#check").click(function(e){
			e.preventDefault();
			var checkin = $('#checkin').val();
			var checkout = $('#checkout').val();
			$.ajax({
				url:"<?php echo site_url('lawns/get_booking_check');?>",
				method:"GET",
				
				data:{checkin:checkin,checkout:checkout},
				success:function(data){
					$('#check_result').html(data);
				}

			});
		});
        </script>

</body>
</html>